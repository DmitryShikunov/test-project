package com.epam.mogilev.training;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener{
    private Logger logger = LogManager.getLogger("Test Listener");

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logger.info("Test " + iTestResult.getMethod().getMethodName() + " was passed");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logger.error("Test " + iTestResult.getMethod().getMethodName() + " was failed");
        ((Tests)iTestResult.getInstance()).saveScreenShot("OnFail");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
