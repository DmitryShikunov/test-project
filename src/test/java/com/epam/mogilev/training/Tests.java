package com.epam.mogilev.training;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Listeners(TestListener.class)
public class Tests {

    private static final String BASE_URL = "https://www.google.by/";

    private WebDriver driver;

    private Logger logger = LogManager.getLogger(this.getClass());

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {

        logger.info("Go to the base page " + BASE_URL);
        driver = BrowserFactory.getDriver();
        Assert.assertNotNull(driver, "Driver wasn't initialised");
        driver.manage().window().maximize();
    }

    @Test(groups = "search", dataProvider = "searc params")
    public void testSearchCanBePerformed(String searchCriteria) {

        driver.navigate().to(BASE_URL);
        GooglePage googlePage = new GooglePage(driver);
        Assert.assertTrue(googlePage.isOpened(), "Google search page wasn't opened");

        googlePage.performSearching(searchCriteria);

        List<WebElement> result = googlePage.getSearchResults();

        Assert.assertTrue(result.size() > 0, "There are no any results");


        Assert.assertTrue(result.get(0).getText().toLowerCase().contains(searchCriteria), "Search result doesn't contain search word: " + searchCriteria);
        colorElement(result.get(0));
        saveScreenShot("");
    }

    public void colorElement(WebElement elem) {

        if (driver instanceof JavascriptExecutor) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='5px solid red'", elem);

        }
    }


    public void saveScreenShot(String fileN) {
        if (driver != null) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy-h-mm-ss-SS--a");
            String formattedDate = sdf.format(date);
            String fileName = (fileN.isEmpty() ? "screenshot-" : fileN) + formattedDate;
            try {
                FileUtils.copyFile(scrFile, new File(String.format("./%s.png", fileName)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @AfterMethod(groups = "search")
    public void cleanup() {
        driver.close();
    }


    @DataProvider(name = "searc params")
    public Object[][] getParameters() {
        return new Object[][]{
                {"some words"},
                {"foo"},
                {"words"}
        };
    }
}
