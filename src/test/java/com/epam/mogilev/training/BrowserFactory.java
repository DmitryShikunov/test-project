package com.epam.mogilev.training;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

    static Logger logger = LogManager.getLogger(BrowserFactory.class);

    public static WebDriver getDriver() {
        String property = System.getProperty("browser");

        System.setProperty("webdriver.gecko.driver", "./geckodriver.exe");

        logger.debug("Starting browser " + property);

        return getDriver(property);
    }

    public static WebDriver getDriver(String browserType) {
        logger.info("Path to driver: " + System.getProperty("webdriver.gecko.driver"));

        switch (browserType) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                logger.warn("There is no case for item " + browserType + " default will be choosen");
                return new ChromeDriver();

        }
    }
}
