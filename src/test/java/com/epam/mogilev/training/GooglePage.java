package com.epam.mogilev.training;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.List;

public class GooglePage {
    private WebDriver driver;
    private static int ELEMENT_TIMEOUT = 5;
    private static final By SEARCH_FIELD_LOCATOR = By.xpath(".//input[@aria-label][@id]");
    private static final By SEARCH_RESULTS_LOCATOR = By.xpath(".//*[@id]//h3/a");
    private static final By RESULTS_STATS_LOCATOR = By.id("resultStats");

    Logger logger = LogManager.getLogger(this.getClass().getSimpleName());

    public GooglePage(WebDriver driver) {
        this.driver = driver;
    }

    public void performSearching(String searchString){
        logger.info("Search by criteria " + searchString);
        try {
            WebElement searchField = driver.findElement(SEARCH_FIELD_LOCATOR);
            logger.debug("Enter " + searchString + " in the Search field");
            searchField.sendKeys(searchString);
            logger.debug("Clicking the Submit button");
            searchField.submit();
        }
        catch (WebDriverException ex){
            logger.error(ex.getMessage());
            Reporter.log("Error in Web Driver");//testng reporter
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }

    }

    public boolean isOpened() {
        return driver.findElements(SEARCH_FIELD_LOCATOR).size() > 0;
    }

    public List<WebElement> getSearchResults(){
        WebDriverWait wait = new WebDriverWait(driver,ELEMENT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(RESULTS_STATS_LOCATOR));
        return driver.findElements(SEARCH_RESULTS_LOCATOR);
    }

}
